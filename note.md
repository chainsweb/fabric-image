before_script:
  - apt-get update -qq && apt-get install -y -qq curl
  - ruby -v
  - which ruby
  - gem install bundler --no-ri --no-rdoc

install_chef:
  script:
    - echo "Install Chef-solo"
    - curl -L https://www.opscode.com/chef/install.sh | bash
    - chef-solo -v
    - chef-solo -c ./chef-repo/solo.rb -j ./chef-repo/web.json


    ansible-galaxy install -p ./roles -r roles.yml

    - apt-add-repository ppa:ansible/ansible
    - apt-get update -qq && apt-get install -y -qq ansible
    - ansible --version


    before_script:
      - wget https://bootstrap.pypa.io/ez_setup.py -O - | python
      - easy_install pip
      - pip install -U pip
      - pip install paramiko PyYAML Jinja2 httplib2 six
      - pip install ansible
      - apt-get update -qq && apt-get install -y -qq curl
      - ansible --version

      - cat /etc/debian_version
      - cat /etc/issue
      - apt-get update -qq
      - apt-get install -y software-properties-common
      - apt-add-repository ppa:ansible/ansible
      - apt-get update && apt-get install -y -qq ansible
      - ansible --version
